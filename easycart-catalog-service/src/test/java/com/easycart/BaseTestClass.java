package com.easycart;

import com.easycart.core.jpa.entity.language.Language;
import com.easycart.core.jpa.repository.core.LanguageRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class BaseTestClass {

    @Autowired
    private LanguageRepository languageRepository;

    @Autowired
    private TestEntityManager entityManager;

    @BeforeAll
    //@Transactional
    public void setup_lang_pack(){
        System.out.println("*****111******");
        Language language = Language.builder().isoCode("en").name("English").build();
        entityManager.persist(language);
        language = Language.builder().isoCode("de").name("German").build();
        entityManager.persist(language);
    }

    public LanguageRepository getLanguageRepository() {
        return languageRepository;
    }

    public void setLanguageRepository(LanguageRepository languageRepository) {
        this.languageRepository = languageRepository;
    }

    public TestEntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(TestEntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
