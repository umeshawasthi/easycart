package com.easycart.jpa.repository.category;

import com.easycart.core.data.category.CategoryDto;
import com.easycart.core.jpa.entity.category.Category;
import com.easycart.core.jpa.entity.language.Language;
import com.easycart.core.jpa.repository.category.CategoryRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import static org.assertj.core.api.Assertions.*;

import org.hibernate.exception.ConstraintViolationException;

@DataJpaTest
public class CategoryRepositoryTest {

    @Autowired
    CategoryRepository categoryRepository;
    
    @Autowired
    private TestEntityManager entityManager;


    @Test
    void saveCategoryDataTest(){
        Category saveBrand= entityManager.persist(Category.builder().code("100").build());
        CategoryDto brand =categoryRepository.findCategoryByCode("100");
        assertThat(brand.getCode()).isEqualTo("100");
    }
    
    @Test
    void duplicateErrorCategoryDataTest(){
    	try
    	{
    		Category saveCategory= entityManager.persist(Category.builder().code("001").build());
    	}
    	catch(ConstraintViolationException ex)
    	{
    		 assertThat(ex.getCause() instanceof ConstraintViolationException);
    	}
        
    }
    
   /* @Test
    void updateCategoryDataTest(){
    	CategoryDto category =categoryRepository.findByCode("001");
    	category=category.builder().name("apparels").build();
        entityManager.persist(category);
        assertThat(category.getName()).isEqualTo("apparels");
    } */
    
   /* @Test
    void parentChildCategoryTest(){
    	Category subcategory1=entityManager.persist(Category.builder().code("100").name("shirt").build());
    	Category subcategory2=entityManager.persist(Category.builder().code("101").name("pants").build());
    	Category category =categoryRepository.findByCode("001");
    	
    	category=category.builder().children(Arrays.asList(subcategory1,subcategory2)).build();
        entityManager.persist(category);
        assertThat(category.getChildren().size()).isEqualTo(2);
    } */
    
    @Test
    void getAllCategoryDataTest(){
    	 //entityManager.persist(Category.builder().code("100").name("shirt").build());
    	 //entityManager.persist(Category.builder().code("101").name("pants").build());
         //Page<Category> category =categoryRepository.findAll();
         //assertThat(category.size()).isEqualTo(3);
    }
    
    @BeforeEach
    public void setup(){
        Language language = Language.builder().isoCode("en").name("English").build();
        entityManager.persist(language);
        language = Language.builder().isoCode("de").name("German").build();
        entityManager.persist(language);
        //entityManager.persist(Category.builder().code("001").name("apparel").build());
        
    }
    
 
}
