package com.easycart.jpa.repository.brand;

import com.easycart.core.jpa.entity.language.Language;
import com.easycart.core.jpa.entity.product.Brand;
import com.easycart.core.jpa.repository.brand.BrandRepository;
import com.easycart.core.jpa.repository.product.ProductRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import static org.assertj.core.api.Assertions.*;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;

@DataJpaTest
public class BrandRepositoryTest {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    BrandRepository brandRepository;
    
    @Autowired
    private TestEntityManager entityManager;


    @Test
    void saveBrandDataTest(){
        Brand saveBrand= entityManager.persist(Brand.builder().code("2").name("nike").build()); 
        Brand brand =brandRepository.findByCode("2");
        assertThat(brand.getCode()).isEqualTo("2");
    }
    
    @Test
    void duplicateErrorBrandDataTest(){
    	try
    	{
            Brand saveBrand= entityManager.persist(Brand.builder().code("1").name("addidas").build()); 	
    	}
    	catch(ConstraintViolationException ex)
    	{
    		 assertThat(ex.getCause() instanceof ConstraintViolationException);
    	}
        
    }
    
    @Test
    void updateBrandDataTest(){
        Brand brand =brandRepository.findByCode("1");
        brand= Brand.builder().name("reebok").build();
        entityManager.persist(brand);
        assertThat(brand.getName()).isEqualTo("reebok");
    }
    
    @Test
    void getAllBrandDataTest(){
    	 entityManager.persist(Brand.builder().code("2").name("nike").build());
    	 entityManager.persist(Brand.builder().code("3").name("test").build());
        List<Brand> brand =brandRepository.findAll();
        assertThat(brand.size()).isEqualTo(3);
    }
    
    @BeforeEach
    public void setup(){
        Language language = Language.builder().isoCode("en").name("English").build();
        entityManager.persist(language);
        language = Language.builder().isoCode("de").name("German").build();
        entityManager.persist(language);
        Brand sampleBrand= entityManager.persist(Brand.builder().code("1").name("nike").build()); 
        
    }
    
 
}
