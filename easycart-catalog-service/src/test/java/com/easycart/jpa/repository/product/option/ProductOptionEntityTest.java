package com.easycart.jpa.repository.product.option;

import com.easycart.BaseTestClass;
import com.easycart.core.jpa.entity.language.Language;
import com.easycart.core.jpa.entity.product.options.ProductOption;
import com.easycart.core.jpa.entity.product.options.localized.LocalizedProductOption;
import com.easycart.core.jpa.repository.core.LanguageRepository;
import com.easycart.core.jpa.repository.product.options.OptionRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;


import java.util.HashMap;

import static org.assertj.core.api.Assertions.*;

@DataJpaTest
public class ProductOptionEntityTest extends BaseTestClass {

    @Autowired
    OptionRepository optionRepository;

    @Autowired
    LanguageRepository languageRepository;


    @Test
    public void test_option_creation(){
        ProductOption productOption = ProductOption.builder().withCode("option_1").withLocOptions(new HashMap())
                .build();

        LocalizedProductOption enOption = LocalizedProductOption.builder().name("Size").build();
        enOption.setLanguage(languageRepository.findByIsoCode("en"));

        LocalizedProductOption deOption = LocalizedProductOption.builder().name("Größe").build();
        deOption.setLanguage(languageRepository.findByIsoCode("de"));

        productOption.addLocalizedOption("en", enOption);
        productOption.addLocalizedOption("de", deOption);
        productOption = getEntityManager().persist(productOption);

        ProductOption perProductOption = optionRepository.findByCode("option_1").orElse(null);

        assertThat(perProductOption).isNotNull();
        assertThat(perProductOption.getCode()).isEqualTo("option_1");
        assertThat(perProductOption.getLocOptions().get("en").getName()).isEqualTo("Size");
        assertThat(perProductOption.getLocOptions().get("de").getName()).isEqualTo("Größe");


    }

    @Test
    public void test_option_for_language(){
        //setup_lang_pack();
        ProductOption productOption = ProductOption.builder().withCode("option_2").withLocOptions(new HashMap())
                .build();

        LocalizedProductOption enOption = LocalizedProductOption.builder().name("Size").build();
        enOption.setLanguage(languageRepository.findByIsoCode("en"));

        LocalizedProductOption deOption = LocalizedProductOption.builder().name("Größe").build();
        deOption.setLanguage(languageRepository.findByIsoCode("de"));

        productOption.addLocalizedOption("en", enOption);
        productOption.addLocalizedOption("de", deOption);
        getEntityManager().persist(productOption);

        ProductOption perProductOption = optionRepository.findByCode("option_2").orElse(null);

        assertThat(perProductOption).isNotNull();
        assertThat(perProductOption.getName("en")).isEqualTo("Size");
        assertThat(perProductOption.getName("nl")).isEmpty();

    }

    @BeforeEach
    public void setup_lang_pack(){
        Language language = Language.builder().isoCode("en").name("English").build();
        getEntityManager().persist(language);
        language = Language.builder().isoCode("de").name("German").build();
        getEntityManager().persist(language);
    }

}
