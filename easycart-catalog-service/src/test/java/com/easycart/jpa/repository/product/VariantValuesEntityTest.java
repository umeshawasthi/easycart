package com.easycart.jpa.repository.product;

import com.easycart.BaseTestClass;
import com.easycart.core.jpa.entity.category.Category;
import com.easycart.core.jpa.entity.language.Language;
import com.easycart.core.jpa.entity.product.Product;
import com.easycart.core.jpa.entity.product.VariantProduct;
import com.easycart.core.jpa.entity.product.VariantValues;
import com.easycart.core.jpa.entity.product.VariantValuesPK;
import com.easycart.core.jpa.entity.product.options.ProductOption;
import com.easycart.core.jpa.entity.product.options.localized.LocalizedProductOption;
import com.easycart.core.jpa.repository.product.options.OptionRepository;
import com.easycart.core.jpa.repository.product.options.OptionValueRepository;
import com.easycart.core.product.ProductStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.HashMap;

@DataJpaTest
public class VariantValuesEntityTest extends BaseTestClass {

    @Autowired
    OptionRepository optionRepository;

    @Autowired
    private OptionValueRepository optionValueRepository;


    @Test
    public void test_create_variant_value(){

    }


    private void create_variant_value_creation() {
        Category cat = getEntityManager().persist(Category.builder().code("2").build());
        Product product = getEntityManager().persist(Product.builder().withCode("123").withStatus(ProductStatus.ACTIVE)
                .withMainCategory(cat)
                .build());

        VariantProduct variantProduct = VariantProduct.builder()
                .product(product)
                .sku("1234_small")
                .status(ProductStatus.ACTIVE).build();

        VariantProduct variantProduct2 = VariantProduct.builder()
                .product(product)
                .sku("1234_medium")
                .status(ProductStatus.ACTIVE).build();

        VariantProduct variantProduct3 = VariantProduct.builder()
                .product(product)
                .sku("1234_large")
                .status(ProductStatus.ACTIVE).build();


        getEntityManager().persist(variantProduct);
        getEntityManager().persist(variantProduct2);
        getEntityManager().persist(variantProduct3);

        ProductOption productOption = optionRepository.findByCode("option_1").orElse(null);

        VariantValues variantValuesSmall = VariantValues.builder()
                .value(optionValueRepository.findByCode("option_value_s").get())
                .id(new VariantValuesPK(productOption, variantProduct)).position(1)
                .build();

        VariantValues variantValuesMedium = VariantValues.builder()
                .value(optionValueRepository.findByCode("option_value_m").get())
                .id(new VariantValuesPK(productOption, variantProduct2)).position(1)
                .build();
        VariantValues variantValuesLarge = VariantValues.builder()
                .value(optionValueRepository.findByCode("option_value_l").get())
                .id(new VariantValuesPK(productOption, variantProduct3)).position(1)
                .build();

         getEntityManager().persist(variantValuesSmall);
         getEntityManager().persist(variantValuesMedium);
         getEntityManager().persist(variantValuesLarge);

    }

    private void createOptionValuesData() {

        ProductOption productOption = ProductOption.builder().withCode("option_1").withLocOptions(new HashMap())
                .build();

        LocalizedProductOption enOption = LocalizedProductOption.builder().name("Size").build();
        enOption.setLanguage(getLanguageRepository().findByIsoCode("en"));

        LocalizedProductOption deOption = LocalizedProductOption.builder().name("Größe").build();
        deOption.setLanguage(getLanguageRepository().findByIsoCode("de"));

        productOption.addLocalizedOption("en", enOption);
        productOption.addLocalizedOption("de", deOption);

        getEntityManager().persist(productOption);

       /*
        ProductOptionValue productOptionValue = ProductOptionValue.builder().code("option_value_s").productOption(productOption)
                .locOptionValues(new HashMap<>()).build();

        LocalizedProductOptionValue enOptionValue = LocalizedProductOptionValue.builder().name("Small").build();
        enOptionValue.setLanguage(getLanguageRepository().findByIsoCode("en"));

        LocalizedProductOptionValue deOptionValue = LocalizedProductOptionValue.builder().name("klein").build();
        deOptionValue.setLanguage(getLanguageRepository().findByIsoCode("de"));

        productOptionValue.addLocalizedProduct("en", enOptionValue);
        productOptionValue.addLocalizedProduct("de", deOptionValue);

        getEntityManager().persist(productOptionValue);

        //medium value
        productOptionValue = ProductOptionValue.builder().code("option_value_m").productOption(productOption)
                .locOptionValues(new HashMap<>()).build();

        enOptionValue = LocalizedProductOptionValue.builder().name("Medium").build();
        enOptionValue.setLanguage(getLanguageRepository().findByIsoCode("en"));

        deOptionValue = LocalizedProductOptionValue.builder().name("Mittel").build();
        deOptionValue.setLanguage(getLanguageRepository().findByIsoCode("de"));

        productOptionValue.addLocalizedProduct("en", enOptionValue);
        productOptionValue.addLocalizedProduct("de", deOptionValue);

        getEntityManager().persist(productOptionValue);

        //large value
        productOptionValue = ProductOptionValue.builder().code("option_value_l").productOption(productOption)
                .locOptionValues(new HashMap<>()).build();

        enOptionValue = LocalizedProductOptionValue.builder().name("Large").build();
        enOptionValue.setLanguage(getLanguageRepository().findByIsoCode("en"));

        deOptionValue = LocalizedProductOptionValue.builder().name("Groß").build();
        deOptionValue.setLanguage(getLanguageRepository().findByIsoCode("de"));

        productOptionValue.addLocalizedProduct("en", enOptionValue);
        productOptionValue.addLocalizedProduct("de", deOptionValue);

        getEntityManager().persist(productOptionValue); */

    }

    @BeforeEach
    public void setup_required_data() {
        setup_lang_pack();
        createOptionValuesData();
        create_variant_value_creation();
    }

    public void setup_lang_pack() {
        Language language = Language.builder().isoCode("en").name("English").build();
        getEntityManager().persist(language);
        language = Language.builder().isoCode("de").name("German").build();
        getEntityManager().persist(language);
    }
}
