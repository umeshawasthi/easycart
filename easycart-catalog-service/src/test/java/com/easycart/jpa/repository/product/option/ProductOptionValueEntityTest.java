package com.easycart.jpa.repository.product.option;

import com.easycart.BaseTestClass;
import com.easycart.core.jpa.entity.language.Language;
import com.easycart.core.jpa.entity.product.options.ProductOption;
import com.easycart.core.jpa.entity.product.options.ProductOptionValue;
import com.easycart.core.jpa.entity.product.options.localized.LocalizedProductOption;
import com.easycart.core.jpa.repository.product.options.OptionRepository;
import com.easycart.core.jpa.repository.product.options.OptionValueRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class ProductOptionValueEntityTest extends BaseTestClass {

    @Autowired
    private OptionValueRepository optionValueRepository;

    @Autowired
    private OptionRepository optionRepository;

    @Test
     public void test_creation_option_values(){
       // assertThat(optionValueRepository.findByCode("option_value_s").get().getCode()).isEqualTo("option_value_s");
     }


    @Test
    public void test_option_values_by_code(){
        ProductOptionValue value= optionValueRepository.findByCode("option_value_s").orElse(null);
        assertThat(value).isNotNull();
        assertThat(value.getName("en")).isEqualTo("Small");
        assertThat(value.getName("it")).isEmpty();
    }

    @Test
    public void find_option_values_by_option(){
        List<ProductOptionValue> values= optionValueRepository.findAllByProductOption(optionRepository.findByCode("option_1").get());
        assertThat(values.size()).isGreaterThanOrEqualTo(1);
        assertThat(values.get(0).getName("de")).isEqualTo("klein");
        assertThat(values.get(0).getName("nl")).isNotEqualTo("test");
    }

    private void createOptionValuesData(){

        ProductOption productOption = ProductOption.builder().withCode("option_1").withLocOptions(new HashMap())
                .build();

        LocalizedProductOption enOption = LocalizedProductOption.builder().name("Size").build();
        enOption.setLanguage(getLanguageRepository().findByIsoCode("en"));

        LocalizedProductOption deOption = LocalizedProductOption.builder().name("Größe").build();
        deOption.setLanguage(getLanguageRepository().findByIsoCode("de"));

        productOption.addLocalizedOption("en", enOption);
        productOption.addLocalizedOption("de", deOption);

        getEntityManager().persist(productOption);


       /* ProductOptionValue productOptionValue = ProductOptionValue.builder().code("option_value_s").productOption(productOption)
                .locOptionValues(new HashMap<>()).build();

        LocalizedProductOptionValue enOptionValue = LocalizedProductOptionValue.builder().name("Small").build();
        enOptionValue.setLanguage(getLanguageRepository().findByIsoCode("en"));

        LocalizedProductOptionValue deOptionValue = LocalizedProductOptionValue.builder().name("klein").build();
        deOptionValue.setLanguage(getLanguageRepository().findByIsoCode("de"));

        productOptionValue.addLocalizedProduct("en", enOptionValue);
        productOptionValue.addLocalizedProduct("de", deOptionValue);

        getEntityManager().persist(productOptionValue); */
    }

    @BeforeEach
    public void setup_required_data(){
        setup_lang_pack();
        createOptionValuesData();
    }

    public void setup_lang_pack(){
        System.out.println("*****1111111111111111******");
        Language language = Language.builder().isoCode("en").name("English").build();
        getEntityManager().persist(language);
        language = Language.builder().isoCode("de").name("German").build();
        getEntityManager().persist(language);
    }
}
