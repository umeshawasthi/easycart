package com.easycart.jpa.repository.product;

import com.easycart.core.jpa.entity.category.Category;
import com.easycart.core.jpa.entity.language.Language;
import com.easycart.core.jpa.entity.product.Brand;
import com.easycart.core.jpa.entity.product.Product;
import com.easycart.core.jpa.entity.product.options.ProductOption;
import com.easycart.core.jpa.entity.product.options.localized.LocalizedProductOption;
import com.easycart.core.jpa.repository.brand.BrandRepository;
import com.easycart.core.jpa.repository.category.CategoryRepository;
import com.easycart.core.jpa.repository.core.LanguageRepository;
import com.easycart.core.jpa.repository.product.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class ProductRepositoryTest {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    CategoryRepository categoryRepository;
    
    @Autowired
    BrandRepository brandRepository;

    @Autowired
    private TestEntityManager entityManager;
    
    @Autowired
    private LanguageRepository languageRepository;

    @Test
    void validate_productData(){
       
        //ProductDto product =productRepository.findByCode("123");
        //assertThat(product.getBrand().getCode()).isEqualTo("1");
    }
    
    @Test
    void validate_brandProductRelation(){
    	Brand brand =brandRepository.findByCode("1");
        List<Product> products =productRepository.findByBrand(brand);
        assertThat(products.size()).isEqualTo(2);
    }
    
//    @Test
//    void validate_brandCategoryRelation(){
//    	Category cat =categoryRepository.findByCode("1001");
//        List<Product> products =productRepository.findByCategory(cat);
//        assertThat(products.size()).isEqualTo(2);
//    }
    
    
    @BeforeEach
    public void setup(){
        Language language = Language.builder().isoCode("en").name("English").build();
        entityManager.persist(language);
        language = Language.builder().isoCode("de").name("German").build();
        entityManager.persist(language);
        
        //option data creation
        createProductOptionData();
        
        //brand data
        createBrandData();
        
        //
        createCategoryData();
        
        //create sample products;
        createProducts();
        
    }
    
    private void createProducts() {
		
    	Brand brand =brandRepository.findByCode("1");
    	//Category cat=categoryRepository.findByCode("1001");
        Category cat = new Category();
    	//Product p1=entityManager.persist(Product.builder().withCode("123").withBrand(brand).withMainCategory(cat).withStatus(ProductStatus.ACTIVE).build());
		
    	//Product p2=entityManager.persist(Product.builder().withCode("1234").withBrand(brand).withMainCategory(cat).withStatus(ProductStatus.ACTIVE).build());
	}


	private void createCategoryData() {
		//Category category=entityManager.persist(Category.builder().code("001").name("apparel").build());
		//Category subCategory=entityManager.persist(Category.builder().code("100").name("Top").parent(category).build());
		//entityManager.persist(Category.builder().code("1001").name("Polo").parent(subCategory).build());
		//entityManager.persist(Category.builder().code("1002").name("T shirt").parent(subCategory).build());
		//entityManager.persist(Category.builder().code("1003").name("Shirt").parent(subCategory).build());
    	//entityManager.persist(Category.builder().code("101").name("Bottom").parent(category).build());
	}


	private void createBrandData() {
		entityManager.persist(Brand.builder().code("1").name("Gap").build());
		entityManager.persist(Brand.builder().code("2").name("Old Navy").build());
		entityManager.persist(Brand.builder().code("3").name("Tommy").build());	
	}


	private void createProductOptionData() {

        ProductOption productOption = ProductOption.builder().withCode("1").withLocOptions(new HashMap())
                .build();

        LocalizedProductOption enOption = LocalizedProductOption.builder().name("Size").build();
        enOption.setLanguage(languageRepository.findByIsoCode("en"));

        LocalizedProductOption deOption = LocalizedProductOption.builder().name("Größe").build();
        deOption.setLanguage(languageRepository.findByIsoCode("de"));

        productOption.addLocalizedOption("en", enOption);
        productOption.addLocalizedOption("de", deOption);

        entityManager.persist(productOption);


       /* ProductOptionValue productOptionValue = ProductOptionValue.builder().code("option_value_s").productOption(productOption)
                .locOptionValues(new HashMap<>()).build();

        LocalizedProductOptionValue enOptionValue = LocalizedProductOptionValue.builder().name("Small").build();
        enOptionValue.setLanguage(languageRepository.findByIsoCode("en"));

        LocalizedProductOptionValue deOptionValue = LocalizedProductOptionValue.builder().name("klein").build();
        deOptionValue.setLanguage(languageRepository.findByIsoCode("de"));

        productOptionValue.addLocalizedProduct("en", enOptionValue);
        productOptionValue.addLocalizedProduct("de", deOptionValue);

        entityManager.persist(productOptionValue);

        //medium value
        productOptionValue = ProductOptionValue.builder().code("option_value_m").productOption(productOption)
                .locOptionValues(new HashMap<>()).build();

        enOptionValue = LocalizedProductOptionValue.builder().name("Medium").build();
        enOptionValue.setLanguage(languageRepository.findByIsoCode("en"));

        deOptionValue = LocalizedProductOptionValue.builder().name("Mittel").build();
        deOptionValue.setLanguage(languageRepository.findByIsoCode("de"));

        productOptionValue.addLocalizedProduct("en", enOptionValue);
        productOptionValue.addLocalizedProduct("de", deOptionValue);

        entityManager.persist(productOptionValue);

        //large value
        productOptionValue = ProductOptionValue.builder().code("option_value_l").productOption(productOption)
                .locOptionValues(new HashMap<>()).build();

        enOptionValue = LocalizedProductOptionValue.builder().name("Large").build();
        enOptionValue.setLanguage(languageRepository.findByIsoCode("en"));

        deOptionValue = LocalizedProductOptionValue.builder().name("Groß").build();
        deOptionValue.setLanguage(languageRepository.findByIsoCode("de"));

        productOptionValue.addLocalizedProduct("en", enOptionValue);
        productOptionValue.addLocalizedProduct("de", deOptionValue);

        entityManager.persist(productOptionValue); */

    }
    
    
}
