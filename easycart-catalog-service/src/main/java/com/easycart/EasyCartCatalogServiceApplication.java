package com.easycart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class EasyCartCatalogServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EasyCartCatalogServiceApplication.class, args);

	}

}
