package com.easycart.core.jpa.entity.product;

import com.easycart.core.jpa.entity.BaseEntity;
import com.easycart.core.jpa.entity.language.Language;
import com.easycart.core.jpa.entity.product.localized.LocalizedProductVariant;
import com.easycart.core.jpa.entity.seo.SEOMeta;
import com.easycart.core.product.ProductStatus;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

@Entity(name = "variant_product")
@Table(name = "variant_products", indexes = {@Index(name = "CODE_VARIANT_INDEX", columnList = "sku")})
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class VariantProduct extends BaseEntity<String> {


    @ManyToOne(fetch = FetchType.LAZY)
   //@JoinColumn(name = "product_id")
    private Product product;

    @Column(unique = true, nullable = false)
    @NotNull
    @EqualsAndHashCode.Include
    private String sku;


    private String externalCode;

    @Enumerated(EnumType.STRING)
    @Column(length = 25)
    private ProductStatus status;

    private long position;

    @Embedded
    private SEOMeta seoMeta;

    @OneToMany(
            mappedBy = "variant",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private Map<Language, LocalizedProductVariant> locVariants = new HashMap<>();

    public void addLocBrand(Language locale, LocalizedProductVariant locVariant){
        locVariants.put(locale, locVariant);
        locVariant.setVariant(this);
    }

    public void removeLocBrand(Language locale, LocalizedProductVariant locVariant){
        locVariants.remove(locale);
        locVariant.setVariant(null);
    }
}
