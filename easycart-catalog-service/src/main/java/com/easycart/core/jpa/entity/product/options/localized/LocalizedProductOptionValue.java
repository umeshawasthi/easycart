package com.easycart.core.jpa.entity.product.options.localized;

import com.easycart.core.jpa.entity.localized.LocalizedBaseEntity;
import com.easycart.core.jpa.entity.product.options.ProductOptionValue;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "loc_product_option_values")
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LocalizedProductOptionValue extends LocalizedBaseEntity<String> {

    private String value;

    @ManyToOne(fetch = FetchType.LAZY)
    private ProductOptionValue productOptionValue;
}
