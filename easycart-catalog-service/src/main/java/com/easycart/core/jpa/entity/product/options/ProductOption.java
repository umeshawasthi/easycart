package com.easycart.core.jpa.entity.product.options;

import com.easycart.core.jpa.entity.BaseEntity;
import com.easycart.core.jpa.entity.product.options.localized.LocalizedProductOption;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity(name = "ProductOption")
@Table(name="product_options")
@Getter @Setter @Builder(setterPrefix = "with")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
public class ProductOption extends BaseEntity<String> {

    @Column(unique = true, nullable = false)
    @EqualsAndHashCode.Include
    private String code;

    //this can be used to sort the options.
    private long position;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.LAZY,
            mappedBy = "productOption"
    )
    private List<ProductOptionValue> productOptionValues = new ArrayList<>();

    public void addOptionValues(ProductOptionValue optionValue){
        productOptionValues.add(optionValue);
        optionValue.setProductOption(this);
    }

    public void removeOptionValues(ProductOptionValue optionValue){
        productOptionValues.remove(optionValue);
        optionValue.setProductOption(null);
    }

    @OneToMany(
            mappedBy = "productOption",
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.LAZY
    )
    @MapKey(name = "languageIsoCode")
    private Map<String, LocalizedProductOption> locOptions = new HashMap<>();

    public String getName(String locale){
        return locOptions.get(locale) !=null ? locOptions.get(locale).getName(): "";
    }

    public void addLocalizedOption(String locale, LocalizedProductOption locOption){
        locOptions.put(locale, locOption);
        locOption.setProductOption(this);
    }

    public void removeLocalizedOption(String locale, LocalizedProductOption locOption){
        locOptions.remove(locale);
        locOption.setProductOption(null);
    }
}
