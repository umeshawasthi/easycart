package com.easycart.core.jpa.repository.product.options;

import com.easycart.core.jpa.entity.product.options.ProductOption;
import com.easycart.core.jpa.entity.product.options.ProductOptionValue;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


@Resource
public interface OptionValueRepository extends PagingAndSortingRepository<ProductOptionValue, Long> {

    List<ProductOptionValue> findAllByProductOption(ProductOption productOption);
    Optional<ProductOptionValue> findByCode(final String code);
}
