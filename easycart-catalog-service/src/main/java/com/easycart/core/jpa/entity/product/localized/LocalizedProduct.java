package com.easycart.core.jpa.entity.product.localized;

import com.easycart.core.jpa.entity.localized.LocalizedBaseEntity;
import com.easycart.core.jpa.entity.product.Product;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "loc_products")
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LocalizedProduct extends LocalizedBaseEntity<String> {


    @Column(nullable = false)
    @NotNull
    private String name;

    @Column(length = 4000)
    @NotNull
    private String description;

    private String summary;

    @ManyToOne(fetch = FetchType.LAZY)
    //@MapsId("id")
    private Product product;
}
