package com.easycart.core.jpa.repository.product;

import com.easycart.core.jpa.entity.product.VariantProduct;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository("variantRepositoryRepository")
public interface ProductVariantRepository extends PagingAndSortingRepository<VariantProduct, Long> {
}
