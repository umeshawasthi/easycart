package com.easycart.core.jpa.repository.core;

import com.easycart.core.jpa.entity.language.Language;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LanguageRepository extends PagingAndSortingRepository<Language, Long> {
    Language findByIsoCode(final String isoCode);
}
