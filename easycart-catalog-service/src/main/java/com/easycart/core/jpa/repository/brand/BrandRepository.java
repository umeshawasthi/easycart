package com.easycart.core.jpa.repository.brand;

import com.easycart.core.data.brand.BrandDto;
import com.easycart.core.data.brand.LocBrandDto;
import com.easycart.core.jpa.entity.product.Brand;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository("brandRepository")
public interface BrandRepository extends PagingAndSortingRepository<Brand,Long> {

	Brand findByCode(String code);
	List<Brand> findAll();

	@Query("select new com.easycart.core.data.brand.LocBrandDto(lb.name, lb.description,lb.language.isoCode) " +
			"from LocalizedBrand lb where lb.brand = :brand")
	List<LocBrandDto> findLocalizedBrandById(@Param("brand") Brand brand);

	@Query(value = "select new com.easycart.core.data.brand.BrandDto(b.id,b.code) FROM Brand b")
	Page<BrandDto> findAllBrands(Pageable pageable);

	Page<Brand> findAll(Pageable pageable);

	@Query("select new com.easycart.core.data.brand.BrandDto(" +
			"b.id,b.code,b.seoMeta, b.createdAt, b.updatedAt)" +
			" FROM Brand b where b.code = :code")
	BrandDto findBrandByCode(@Param("code") String code);

}
