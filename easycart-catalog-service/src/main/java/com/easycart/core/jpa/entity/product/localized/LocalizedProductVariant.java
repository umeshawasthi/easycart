package com.easycart.core.jpa.entity.product.localized;

import com.easycart.core.jpa.entity.localized.LocalizedPK;
import com.easycart.core.jpa.entity.product.VariantProduct;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "loc_product_variants")
@Getter @Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class LocalizedProductVariant implements Serializable {

    @EmbeddedId
    LocalizedPK id;

    @Column(nullable = false)
    @NotNull
    private String name;

    @Column(length = 4000)
    private String description;

    private String summary;

    @ManyToOne(fetch = FetchType.LAZY)
    private VariantProduct variant;
}
