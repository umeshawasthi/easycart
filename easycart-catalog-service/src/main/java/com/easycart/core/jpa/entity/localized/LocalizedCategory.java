package com.easycart.core.jpa.entity.localized;

import com.easycart.core.jpa.entity.category.Category;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "loc_categories")
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class LocalizedCategory extends LocalizedBaseEntity<String> {


    private String name;

    @Column(length = 4000)
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    private Category category;
}
