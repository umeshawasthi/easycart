package com.easycart.core.jpa.repository.category;

import com.easycart.core.data.category.CategoryDto;
import com.easycart.core.data.category.LocCategoryDto;
import com.easycart.core.jpa.entity.category.Category;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository("categoryRepository")
public interface CategoryRepository extends PagingAndSortingRepository<Category,Long> {

	@Query("select new com.easycart.core.data.category.CategoryDto(" +
			"c.id,c.code,c.externalCode,c.seoMeta, c.createdAt, c.updatedAt,parent.code)" +
			" FROM Category c left join c.parent as parent where c.code = :code")
	CategoryDto findCategoryByCode(@Param("code") String code);

	Optional<Category> findByCode(String code);

	@Query(value = "select new com.easycart.core.data.category.CategoryDto(c.id,c.code) FROM Category c")
	Page<CategoryDto> findAllCategories(Pageable pageable);

	Page<Category> findAll(Pageable pageable);

	@Query("select new com.easycart.core.data.category.LocCategoryDto(" +
			"lc.name, lc.description, lc.language.isoCode) " +
			"from LocalizedCategory lc where lc.category = :category")
	List<LocCategoryDto> findLocalizedCategoryById(@Param("category") Category category);

	@Query("select new com.easycart.core.data.category.LocCategoryDto(lc.name, lc.language.isoCode) " +
			"from LocalizedCategory lc where lc.category = :category")
	List<LocCategoryDto> findLocalizedCategoryByCategory(@Param("category") Category category);

}
