package com.easycart.core.jpa.repository.product;

import com.easycart.core.data.product.LocProductDto;
import com.easycart.core.data.product.ProductDto;
import com.easycart.core.jpa.entity.language.Language;
import com.easycart.core.jpa.entity.product.Brand;
import com.easycart.core.jpa.entity.product.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("productRepository")
public interface ProductRepository extends PagingAndSortingRepository<Product,Long> {


    @Query("select new com.easycart.core.data.product.ProductDto(" +
            "p.id,p.code,p.externalCode,p.seoMeta,p.status,p.createdAt,p.updatedAt)" +
            " FROM product p where p.code = :code")
    ProductDto findProductByCode(@Param("code") final String code);

    @Query("select new com.easycart.core.data.product.LocProductDto(" +
            "lp.name, lp.description, lp.language.isoCode) " +
            "from LocalizedProduct lp where lp.product = :product" +
            " AND lp.language = :language")
    List<LocProductDto> findLocalizedProductByLanguage(@Param("product") Product product, final @Param("language")Language language);

    @Query("select new com.easycart.core.data.product.LocProductDto(" +
            "lp.name, lp.description, lp.language.isoCode) " +
            "from LocalizedProduct lp where lp.product = :product")
    List<LocProductDto> findLocalizedProductByCode(@Param("product") Product product);

    List<Product> findByBrand(final Brand brand);

}
