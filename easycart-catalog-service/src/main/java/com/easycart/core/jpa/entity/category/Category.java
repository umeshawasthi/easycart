package com.easycart.core.jpa.entity.category;

import com.easycart.core.jpa.entity.BaseEntity;
import com.easycart.core.jpa.entity.localized.LocalizedCategory;
import com.easycart.core.jpa.entity.seo.SEOMeta;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "categories")
@Builder @Setter @Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Category extends BaseEntity<String> {

    @Column(unique = true, nullable = false)
    @NotNull
    private String code;

    private String externalCode;

    //private String name;

    @ManyToOne(cascade = CascadeType.ALL)
    private Category parent;

    @OneToMany(mappedBy="parent")
    private Collection<Category> children;

    @Embedded
    private SEOMeta seoMeta;

    @OneToMany(
            mappedBy = "category",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private Map<String, LocalizedCategory> locCategories = new HashMap<>();

    public void addLocCategory(String locale, LocalizedCategory locCategory){
        locCategories.put(locale, locCategory);
        locCategory.setCategory(this);
    }

    public void removeLocCategory(String locale, LocalizedCategory locCategory){
        locCategories.remove(locale);
        locCategory.setCategory(null);
    }

}
