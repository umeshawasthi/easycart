package com.easycart.core.jpa.entity.language;

import com.easycart.core.jpa.entity.BaseEntity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity(name = "language")
@Table(name = "languages")
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Language extends BaseEntity<String> {

    @EqualsAndHashCode.Include
    @NotNull
    @Column(unique = true, nullable = false)
    private String isoCode;
    private boolean active;
    private String name;
}
