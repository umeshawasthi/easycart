package com.easycart.core.jpa.entity.product.options;

import com.easycart.core.jpa.entity.BaseEntity;
import com.easycart.core.jpa.entity.product.options.localized.LocalizedProductOptionValue;
import lombok.*;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity(name = "ProductOptionValue")
@Table(name = "product_option_values")
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ProductOptionValue extends BaseEntity<String> {

    @ManyToOne(fetch = FetchType.LAZY)
    private ProductOption productOption;

    //@Column(unique = true, nullable = false)
    private String code;
    
    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.LAZY
    )
    @MapKey(name = "languageIsoCode")
    private Map<String, LocalizedProductOptionValue> locOptionValues = new HashMap<>();

    public void addLocalizedOptionValues(String locale, LocalizedProductOptionValue locOptionValue){
        locOptionValues.put(locale, locOptionValue);
        locOptionValue.setProductOptionValue(this);
    }

    public void removeLocalizedOptionValues(String locale, LocalizedProductOptionValue locOptionValue){
        locOptionValues.remove(locale);
        locOptionValue.setProductOptionValue(null);
    }

    public String getName(String locale){
        return locOptionValues.get(locale) !=null ? locOptionValues.get(locale).getValue(): "";
    }
}
