package com.easycart.core.jpa.entity.product;

import com.easycart.core.jpa.entity.BaseEntity;
import com.easycart.core.jpa.entity.category.Category;
import com.easycart.core.jpa.entity.language.Language;
import com.easycart.core.jpa.entity.product.localized.LocalizedProduct;
import com.easycart.core.jpa.entity.seo.SEOMeta;
import com.easycart.core.product.ProductStatus;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity(name = "product")
@Getter @Setter
@Builder(setterPrefix = "with")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Table(name = "products", indexes = {@Index(name = "CODE_INDEX", columnList = "code")})
public class Product extends BaseEntity<String> {

    @Column(unique = true, nullable = false)
    @NotNull
    @EqualsAndHashCode.Include
    private String code;

    private String externalCode;


    @Enumerated(EnumType.STRING)
    @Column(length = 25)
    @NotNull
    private ProductStatus status;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "main_category")
    @NotNull
    private Category mainCategory;

    @Embedded
    private SEOMeta seoMeta;

    @OneToOne
    @JoinColumn(name = "brand_code")
    private Brand brand;

    @OneToMany(
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            orphanRemoval = true,
            mappedBy = "product"
    )
    //@JoinColumn(name = "variant_id")
    private List<VariantProduct> variantProducts= new ArrayList<>();

    public void addVariant(VariantProduct variant){
        variantProducts.add(variant);
        variant.setProduct(this);
    }

    public void removeVariant(VariantProduct variant){
        variantProducts.remove(variant);
        variant.setProduct(null);
    }

    @OneToMany(
            mappedBy = "product",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private Map<String, LocalizedProduct> locProducts = new HashMap<>();

    public void addLocProducts(String locale, LocalizedProduct lolProduct){
        locProducts.put(locale, lolProduct);
        lolProduct.setProduct(this);
    }

    public void removeLocProducts(String locale, LocalizedProduct lolProduct){
        locProducts.remove(locale);
        lolProduct.setProduct(null);
    }

}
