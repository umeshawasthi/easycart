package com.easycart.core.jpa.entity.product;

import com.easycart.core.jpa.entity.Auditable;
import com.easycart.core.jpa.entity.product.options.ProductOptionValue;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "variant_values")
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VariantValues extends Auditable<String> {

    @EmbeddedId
    VariantValuesPK id;

    private int position;

    //TODO check this
    @ManyToOne
    @JoinColumns({@JoinColumn(name = "option_value_code", referencedColumnName = "code")})
    private ProductOptionValue value;

}

