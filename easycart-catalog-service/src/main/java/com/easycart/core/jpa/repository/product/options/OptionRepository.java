package com.easycart.core.jpa.repository.product.options;


import com.easycart.core.data.product.LocOptionDto;
import com.easycart.core.data.product.OptionDto;
import com.easycart.core.jpa.entity.product.options.ProductOption;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OptionRepository extends PagingAndSortingRepository<ProductOption, Long> {

    Optional<ProductOption> findByCode(final String code);

    Page<ProductOption> findAll(final Pageable pageable);

    @Query(value = "select new com.easycart.core.data.product.OptionDto(o.id,o.code, o.position) FROM ProductOption o")
    Page<ProductOption> findAllOptions(final Pageable pageable);

    @Query("select new com.easycart.core.data.product.OptionDto(" +
            "o.id,o.code, o.position,o.createdAt, o.updatedAt)" +
            " FROM ProductOption o where o.code = :code")
    OptionDto findOptionByCode(@Param("code") String code);


    @Query("select new com.easycart.core.data.product.LocOptionDto(lo.name ,lo.language.isoCode) " +
            "from LocalizedProductOption lo where lo.productOption = :productionOption")
    List<LocOptionDto> findLocalizedProductOptionsById(@Param("productionOption") ProductOption productionOption);
}
