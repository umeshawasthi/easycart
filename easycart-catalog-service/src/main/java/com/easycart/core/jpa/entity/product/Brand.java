package com.easycart.core.jpa.entity.product;

import com.easycart.core.jpa.entity.BaseEntity;
import com.easycart.core.jpa.entity.product.localized.LocalizedBrand;

import lombok.*;
import com.easycart.core.jpa.entity.seo.SEOMeta;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "brands")
@Getter @Setter @Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Brand extends BaseEntity<String> {

    @Column(unique = true, nullable = false)
    @EqualsAndHashCode.Include
    private String code;
    
    private String name;

    @Embedded
    private SEOMeta seoMeta;

    @OneToMany(
            mappedBy = "brand",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private Map<String, LocalizedBrand> locBrand = new HashMap<>();

    public void addLocBrand(String locale, LocalizedBrand brand){
        locBrand.put(locale, brand);
        brand.setBrand(this);
    }

    public void removeLocBrand(String locale, LocalizedBrand brand){
        locBrand.remove(locale);
        brand.setBrand(null);
    }
}
