package com.easycart.core.jpa.entity.product.options.localized;

import com.easycart.core.jpa.entity.localized.LocalizedBaseEntity;
import com.easycart.core.jpa.entity.product.options.ProductOption;
import lombok.*;
import javax.persistence.*;


@Getter @Setter
@EqualsAndHashCode
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="loc_product_options")
public class LocalizedProductOption extends LocalizedBaseEntity<String> {

    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    private ProductOption productOption;

}
