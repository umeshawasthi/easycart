package com.easycart.core.jpa.entity.localized;

import com.easycart.core.jpa.entity.BaseEntity;
import com.easycart.core.jpa.entity.language.Language;
import lombok.*;
import javax.persistence.*;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
@AllArgsConstructor
public class LocalizedBaseEntity<T> extends BaseEntity<T> {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "locale",
            referencedColumnName = "isoCode"
    )
    @NonNull
    private Language language;

    @Column(name = "language_code", insertable = false, updatable = false)
    private String languageIsoCode;

    public void setLanguage(Language language) {
        this.languageIsoCode = language.getIsoCode();
        this.language = language;
    }
}
