package com.easycart.core.jpa.entity;

import lombok.*;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

@MappedSuperclass
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class BaseEntity<T> extends Auditable<T> {

    @Id
    @GeneratedValue
    @EqualsAndHashCode.Include
    private Long id;

    @Version
    private int version;
}
