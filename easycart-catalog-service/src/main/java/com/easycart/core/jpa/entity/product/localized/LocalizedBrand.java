package com.easycart.core.jpa.entity.product.localized;

import com.easycart.core.jpa.entity.localized.LocalizedBaseEntity;
import com.easycart.core.jpa.entity.product.Brand;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "loc_brands")
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class LocalizedBrand extends LocalizedBaseEntity<String> {


    private String name;
    @Column(length = 4000)
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    private Brand brand;

}
