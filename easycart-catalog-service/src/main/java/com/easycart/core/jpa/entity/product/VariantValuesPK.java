package com.easycart.core.jpa.entity.product;

import com.easycart.core.jpa.entity.product.options.ProductOption;
import lombok.*;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class VariantValuesPK implements Serializable {

    @ManyToOne
    @JoinColumn(name = "product_option_code")
    private ProductOption productOption;

    @ManyToOne
//    @JoinColumns({@JoinColumn(name = "variant_code", referencedColumnName = "id"),
//                    @JoinColumn(name = "product_code", referencedColumnName = "product_id")})
    @JoinColumns({@JoinColumn(name = "variant_code", referencedColumnName = "id")})
    private VariantProduct variant;
}
