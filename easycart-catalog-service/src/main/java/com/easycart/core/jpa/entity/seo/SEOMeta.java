package com.easycart.core.jpa.entity.seo;

import lombok.*;

import javax.persistence.Embeddable;


@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter @Builder
public class SEOMeta {

    private String metaKeyword;
    private String metaDescription;
    private String slug;
}
