package com.easycart.core.convertor.product.option;

import com.easycart.core.convertor.LocalizedDataConverter;
import com.easycart.core.data.product.LocOptionDto;
import com.easycart.core.jpa.entity.product.options.localized.LocalizedProductOption;
import com.easycart.core.jpa.repository.core.LanguageRepository;
import com.easycart.core.jpa.repository.product.options.OptionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("locOptionConvertor")
@Slf4j
public class LocProductOptionConvertor implements LocalizedDataConverter<LocOptionDto, LocalizedProductOption> {

    @Resource
    LanguageRepository languageRepository;

    @Resource
    private OptionRepository optionRepository;

    @Override
    public LocalizedProductOption convert(LocOptionDto source, LocalizedProductOption target) {
        LocalizedProductOption locOption = LocalizedProductOption.builder()
                .name(source.getName())
                .build();

        locOption.setLanguage(languageRepository.findByIsoCode(source.getIsoCode()));
        return locOption;
    }

    @Override
    public LocalizedProductOption convert(LocOptionDto source) {
        LocalizedProductOption locOption=  LocalizedProductOption.builder()
                .name(source.getName())
                .build();
        locOption.setLanguage(languageRepository.findByIsoCode(source.getIsoCode()));
        return locOption;
    }

    @Override
    public LocOptionDto reverseConvertor(LocalizedProductOption target) {
        return LocOptionDto.builder()
                .name(target.getName())
                .isoCode(getStringVal(target.getLanguage().getIsoCode()))
                .build();
    }
}
