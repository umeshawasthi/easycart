package com.easycart.core.convertor;

import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.List;

public interface Converter<S, T>{

    T convertToEntity(S source);
    S convertToDto(T source);
    List<T> convertToEntities(Collection<S> dtos);
    List<S> convertToDtos(Collection<T> entities);

    default String getStringVal(final String val){
        return StringUtils.isNotEmpty(val) ? val : StringUtils.EMPTY;
    }
}
