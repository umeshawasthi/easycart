package com.easycart.core.convertor.product.option;

import com.easycart.core.convertor.Converter;
import com.easycart.core.data.product.OptionDto;
import com.easycart.core.jpa.entity.product.options.ProductOption;
import com.easycart.core.jpa.entity.product.options.ProductOptionValue;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Component(value = "optionConvertor")
public class ProductOptionConvertor implements Converter<OptionDto, ProductOption> {


    @Resource
    private LocProductOptionValueConvertor locOptionValueConvertor;

    @Override
    public ProductOption convertToEntity(OptionDto source) {
        ProductOption productOption=  ProductOption.builder()
                .withCode(source.getCode())
                .withPosition(source.getPosition())
                .withLocOptions(new HashMap<>())
                .withProductOptionValues(new ArrayList<>())
                .build();
        productOption.addOptionValues(populateProductOptionValues(source));

        return productOption;
    }

    @Override
    public OptionDto convertToDto(ProductOption source) {
        return OptionDto.builder()
                .code(source.getCode())
                .position(source.getPosition())
                .build();
    }

    @Override
    public List<ProductOption> convertToEntities(Collection<OptionDto> dtos) {
        return dtos.stream().map(this::convertToEntity).collect(Collectors.toList());
    }

    @Override
    public List<OptionDto> convertToDtos(Collection<ProductOption> entities) {
        return entities.stream().map(this::convertToDto).collect(Collectors.toList());
    }

    private ProductOptionValue populateProductOptionValues(OptionDto source){
        ProductOptionValue optionValue=  ProductOptionValue.builder()
                .code(source.getCode())
                .locOptionValues(new HashMap<>())
                .build();

        source.getValues().forEach(m -> {
            m.getCode();
            m.getTranslation().forEach((isoCode, oDto) -> {
                    oDto.setIsoCode(isoCode);
                    optionValue.addLocalizedOptionValues(isoCode, locOptionValueConvertor.convert(oDto));

            });
        });
        return optionValue;
    }
}
