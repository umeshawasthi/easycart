package com.easycart.core.convertor.product;

import com.easycart.core.convertor.Converter;
import com.easycart.core.data.product.ProductDto;
import com.easycart.core.jpa.entity.product.Product;
import com.easycart.core.jpa.entity.seo.SEOMeta;
import com.easycart.core.jpa.repository.brand.BrandRepository;
import com.easycart.core.jpa.repository.category.CategoryRepository;
import com.easycart.core.product.ProductStatus;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

@Service("productConvertor")
public class ProductConvertor implements Converter<ProductDto, Product> {

    @Resource
    private CategoryRepository categoryRepository;

    @Resource
    private BrandRepository brandRepository;

    @Override
    public Product convertToEntity(ProductDto source) {

        Product product= Product.builder()
                .withCode(source.getCode())
                .withExternalCode(getStringVal(source.getExternalCode()))
                .withSeoMeta(SEOMeta.builder()
                .metaDescription(getStringVal(source.getSeoMeta().getMetaDescription()))
                .metaKeyword(getStringVal(source.getSeoMeta().getMetaKeyword()))
                .slug(getStringVal(source.getSeoMeta().getSlug()))
                .build())
                .withLocProducts(new HashMap<>())
                .build();

        ProductStatus status = source.getStatus()!=null ? source.getStatus() : ProductStatus.DRAFT;
        product.setStatus(status);
        populateCategoryData(source, product);
        populateBrandData(source, product);
        return product;
    }

    @Override
    public ProductDto convertToDto(Product source) {
        return ProductDto.builder().code(source.getCode())
                .externalCode(getStringVal(source.getExternalCode()))
                .seoMeta(SEOMeta.builder()
                        .metaDescription(getStringVal(source.getSeoMeta().getMetaDescription()))
                        .metaKeyword(getStringVal(source.getSeoMeta().getMetaKeyword()))
                        .slug(getStringVal(source.getSeoMeta().getSlug()))
                        .build())
                .build();
    }

    @Override
    public List<Product> convertToEntities(Collection<ProductDto> dtos) {
        return null;
    }

    @Override
    public List<ProductDto> convertToDtos(Collection<Product> entities) {
        return null;
    }

    private void populateCategoryData(final ProductDto source, Product product){
        if(StringUtils.isNotEmpty(source.getMainCategory())){
            product.setMainCategory( categoryRepository.findByCode(source.getMainCategory()).orElse(null));
        }
    }

    private void populateBrandData(final ProductDto source, Product product) {
        if (StringUtils.isNotEmpty(source.getBrandCode())) {
            product.setBrand(brandRepository.findByCode(source.getBrandCode()));
        }
    }
}
