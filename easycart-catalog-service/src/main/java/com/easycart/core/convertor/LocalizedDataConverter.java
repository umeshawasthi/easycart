package com.easycart.core.convertor;

import org.apache.commons.lang3.StringUtils;

public interface LocalizedDataConverter <S, T>{

    T convert(S source, T target);
    T convert(S source);
    S reverseConvertor(T target);
    default String getStringVal(final String val){
        return StringUtils.isNotEmpty(val) ? val : StringUtils.EMPTY;
    }
}
