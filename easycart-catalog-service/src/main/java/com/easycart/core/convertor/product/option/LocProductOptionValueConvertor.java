package com.easycart.core.convertor.product.option;

import com.easycart.core.convertor.LocalizedDataConverter;
import com.easycart.core.data.product.LocOptionValueMapDto;
import com.easycart.core.jpa.entity.product.options.localized.LocalizedProductOptionValue;
import com.easycart.core.jpa.repository.core.LanguageRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("locOptionValueConvertor")
@Slf4j
public class LocProductOptionValueConvertor implements LocalizedDataConverter<LocOptionValueMapDto, LocalizedProductOptionValue> {

    @Resource
    LanguageRepository languageRepository;

    @Override
    public LocalizedProductOptionValue convert(LocOptionValueMapDto source, LocalizedProductOptionValue target) {
        return null;
    }

    @Override
    public LocalizedProductOptionValue convert(LocOptionValueMapDto source) {
        LocalizedProductOptionValue valueOption = LocalizedProductOptionValue.builder()
                .value(source.getValue())
                .build();

        valueOption.setLanguage(languageRepository.findByIsoCode(source.getIsoCode()));
        return valueOption;
    }

    @Override
    public LocOptionValueMapDto reverseConvertor(LocalizedProductOptionValue target) {
        return null;
    }
}
