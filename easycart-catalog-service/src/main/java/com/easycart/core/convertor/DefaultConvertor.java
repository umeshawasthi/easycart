package com.easycart.core.convertor;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DefaultConvertor<S, T> implements Converter<S, T>{

    private final Function<S, T> toEntity;
    private final Function<T, S> toDto;

    public DefaultConvertor(final Function<S, T> toEntity, final Function<T, S> toDto) {
        this.toEntity = toEntity;
        this.toDto = toDto;
    }

    @Override
    public T convertToEntity(S dto) {
        return toEntity.apply(dto);
    }

    @Override
    public S convertToDto(T entity) {
        return toDto.apply(entity);
    }

    @Override
    public List<T> convertToEntities(Collection<S> dtos) {
        return dtos.stream().map(this::convertToEntity).collect(Collectors.toList());
    }

    @Override
    public List<S> convertToDtos(Collection<T> entities) {
        return entities.stream().map(this::convertToDto).collect(Collectors.toList());
    }

}
