package com.easycart.core.convertor.category;

import com.easycart.core.convertor.Converter;
import com.easycart.core.data.category.CategoryDto;
import com.easycart.core.jpa.entity.category.Category;
import com.easycart.core.jpa.entity.seo.SEOMeta;
import com.easycart.core.jpa.repository.category.CategoryRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


@Component(value = "categoryConvertor")
public class CategoryConvertor implements Converter<CategoryDto, Category> {


    @Resource
    CategoryRepository categoryRepository;

    @Override
    public CategoryDto convertToDto(Category category){
        return CategoryDto.builder().code(category.getCode())
                .externalCode(getStringVal(category.getExternalCode()))
                 .seoMeta(SEOMeta.builder()
                         .metaDescription(getStringVal(category.getSeoMeta().getMetaDescription()))
                         .metaKeyword(getStringVal(category.getSeoMeta().getMetaKeyword()))
                         .slug(getStringVal(category.getSeoMeta().getSlug()))
                         .build())
                .build();
    }


    @Override
    public Category convertToEntity(CategoryDto data){

        return Category.builder().code(data.getCode())
                .externalCode(getStringVal(data.getExternalCode()))
                .locCategories(new HashMap())
                 .parent(setParentCategory(data))
                .seoMeta(metaBuilder(data))
                .build();
    }

    private SEOMeta metaBuilder(CategoryDto data){
        if(Objects.nonNull(data.getSeoMeta())){
            return SEOMeta.builder()
                    .metaDescription(getStringVal(data.getSeoMeta().getMetaDescription()))
                    .metaKeyword(getStringVal(data.getSeoMeta().getMetaKeyword()))
                    .slug(getStringVal(data.getSeoMeta().getSlug()))
                    .build();
        }
        return null;
    }

    @Override
    public List<Category> convertToEntities(Collection<CategoryDto> dtos) {
        return dtos.stream().map(this::convertToEntity).collect(Collectors.toList());
    }

    @Override
    public List<CategoryDto> convertToDtos(Collection<Category> entities) {
        return entities.stream().map(this::convertToDto).collect(Collectors.toList());
    }

    private Category setParentCategory(CategoryDto data){
        if(Objects.nonNull(data.getParent())){
            if(StringUtils.isNotEmpty(data.getParent().getCode())){
                return categoryRepository.findByCode(data.getParent().getCode()).orElse(convertToEntity(data.getParent()));
            }
        }
        return null;
    }
}
