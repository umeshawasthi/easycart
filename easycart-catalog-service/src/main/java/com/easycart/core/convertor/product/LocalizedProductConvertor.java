package com.easycart.core.convertor.product;

import com.easycart.core.convertor.LocalizedDataConverter;
import com.easycart.core.data.product.LocProductDto;
import com.easycart.core.jpa.entity.product.localized.LocalizedProduct;
import com.easycart.core.jpa.repository.core.LanguageRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("locProductConvertor")
public class LocalizedProductConvertor implements LocalizedDataConverter<LocProductDto, LocalizedProduct> {

    @Resource
    LanguageRepository languageRepository;



    @Override
    public LocalizedProduct convert(LocProductDto source, LocalizedProduct target) {

        LocalizedProduct product=  LocalizedProduct.builder()
                .description(source.getDescription())
                .name(source.getName())
                .build();

        product.setLanguage(languageRepository.findByIsoCode(source.getIsoCode()));
        return product;
    }

    @Override
    public LocalizedProduct convert(LocProductDto source) {
        LocalizedProduct product=  LocalizedProduct.builder()
                .description(source.getDescription())
                .name(source.getName())
                .build();
        product.setLanguage(languageRepository.findByIsoCode(source.getIsoCode()));
        return product;
    }

    @Override
    public LocProductDto reverseConvertor(LocalizedProduct target) {
        return null;
    }
}
