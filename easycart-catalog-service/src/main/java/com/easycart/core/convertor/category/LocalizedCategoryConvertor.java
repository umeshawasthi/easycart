package com.easycart.core.convertor.category;

import com.easycart.core.convertor.LocalizedDataConverter;
import com.easycart.core.data.category.LocCategoryDto;
import com.easycart.core.jpa.entity.category.Category;
import com.easycart.core.jpa.entity.localized.LocalizedCategory;
import com.easycart.core.jpa.repository.category.CategoryRepository;
import com.easycart.core.jpa.repository.core.LanguageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service("locCategoryConvertor")
public class LocalizedCategoryConvertor implements LocalizedDataConverter<LocCategoryDto, LocalizedCategory> {

    @Autowired
    LanguageRepository languageRepository;

    @Resource
    CategoryRepository categoryRepository;

    @Override
    public LocalizedCategory convert(LocCategoryDto source, LocalizedCategory target) {
        LocalizedCategory locCategory=  LocalizedCategory.builder()
                .description(getStringVal(source.getDescription()))
                .name(source.getName())
                .build();

        locCategory.setLanguage(languageRepository.findByIsoCode(source.getIsoCode()));
        return locCategory;
    }

    @Override
    public LocalizedCategory convert(LocCategoryDto source) {
        LocalizedCategory locCategory=  LocalizedCategory.builder()
                .description(getStringVal(source.getDescription()))
                .name(source.getName())
                .build();

        locCategory.setLanguage(languageRepository.findByIsoCode(source.getIsoCode()));
        return locCategory;
    }


    @Override
    public LocCategoryDto reverseConvertor(LocalizedCategory target) {
        return LocCategoryDto.builder()
                .name(getStringVal(target.getName()))
                .description(getStringVal(target.getDescription()))
                .isoCode(getStringVal(target.getLanguage().getIsoCode()))
                .build();
    }

    private Map<String, LocCategoryDto> populateLocalizedData(Category cat){

        List<LocCategoryDto> locCategories = categoryRepository.findLocalizedCategoryById(cat);
        return locCategories.stream().collect(Collectors.toMap( LocCategoryDto::getIsoCode, Function.identity()));
    }

    public Map<String, LocCategoryDto> populateLocalizedData(Long id){
        Category cat = new Category();
        cat.setId(id);
        return populateLocalizedData(cat);
    }
}
