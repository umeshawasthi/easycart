package com.easycart.core.convertor.brand;

import com.easycart.core.convertor.Converter;
import com.easycart.core.data.brand.BrandDto;

import com.easycart.core.jpa.entity.product.Brand;
import com.easycart.core.jpa.entity.seo.SEOMeta;
import com.easycart.core.jpa.repository.category.CategoryRepository;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component(value = "brandConvertor")
public class BrandConvertor implements Converter<BrandDto, Brand> {

    @Resource
    CategoryRepository categoryRepository;

    @Override
    public BrandDto convertToDto(Brand brand){
        return BrandDto.builder().code(brand.getCode())
                .seoMeta(SEOMeta.builder()
                        .metaDescription(getStringVal(brand.getSeoMeta().getMetaDescription()))
                        .metaKeyword(getStringVal(brand.getSeoMeta().getMetaKeyword()))
                        .slug(getStringVal(brand.getSeoMeta().getSlug()))
                        .build())
                 .build();

    }


    @Override
    public Brand convertToEntity(BrandDto data){

        return Brand.builder().code(data.getCode())
                .seoMeta(metaBuilder(data))
                 .locBrand(new HashMap())
                .build();
    }

    private SEOMeta metaBuilder(BrandDto data){
        if(Objects.nonNull(data.getSeoMeta())){
            return SEOMeta.builder()
                    .metaDescription(getStringVal(data.getSeoMeta().getMetaDescription()))
                    .metaKeyword(getStringVal(data.getSeoMeta().getMetaKeyword()))
                    .slug(getStringVal(data.getSeoMeta().getSlug()))
                    .build();
        }
        return null;
    }

    @Override
    public List<Brand> convertToEntities(Collection<BrandDto> dtos) {
        return dtos.stream().map(this::convertToEntity).collect(Collectors.toList());
    }

    @Override
    public List<BrandDto> convertToDtos(Collection<Brand> entities) {
        return entities.stream().map(this::convertToDto).collect(Collectors.toList());
    }
}
