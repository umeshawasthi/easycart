package com.easycart.core.convertor.brand;

import com.easycart.core.convertor.LocalizedDataConverter;
import com.easycart.core.data.brand.LocBrandDto;
import com.easycart.core.jpa.entity.product.Brand;
import com.easycart.core.jpa.entity.product.localized.LocalizedBrand;
import com.easycart.core.jpa.repository.brand.BrandRepository;
import com.easycart.core.jpa.repository.core.LanguageRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service("locBrandConvertor")
public class LocalizedBrandConvertor implements LocalizedDataConverter<LocBrandDto, LocalizedBrand> {

    @Resource
    LanguageRepository languageRepository;

    @Resource
    BrandRepository brandRepository;

    @Override
    public LocalizedBrand convert(LocBrandDto source, LocalizedBrand target) {
        LocalizedBrand locBrand=  LocalizedBrand.builder()
                .description(getStringVal(source.getDescription()))
                .name(source.getName())
                .build();

        locBrand.setLanguage(languageRepository.findByIsoCode(source.getIsoCode()));
        return locBrand;
    }

    @Override
    public LocalizedBrand convert(LocBrandDto source) {
        LocalizedBrand locBrand=  LocalizedBrand.builder()
                .description(getStringVal(source.getDescription()))
                .name(source.getName())
                .build();

        locBrand.setLanguage(languageRepository.findByIsoCode(source.getIsoCode()));
        return locBrand;
    }

    @Override
    public LocBrandDto reverseConvertor(LocalizedBrand target) {
        return LocBrandDto.builder()
                .name(getStringVal(target.getName()))
                .description(getStringVal(target.getDescription()))
                .isoCode(getStringVal(target.getLanguage().getIsoCode()))
                .build();
    }

    private Map<String, LocBrandDto> populateLocalizedData(Brand brand){

        List<LocBrandDto> locBrands = brandRepository.findLocalizedBrandById(brand);
        return locBrands.stream().collect(Collectors.toMap( LocBrandDto::getIsoCode, Function.identity()));
    }

    public Map<String, LocBrandDto> populateLocalizedData(Long id){
        Brand brand = new Brand();
        brand.setId(id);
        return populateLocalizedData(brand);
    }
}
