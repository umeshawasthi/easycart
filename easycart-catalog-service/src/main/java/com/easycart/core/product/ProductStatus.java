package com.easycart.core.product;

public enum ProductStatus {

    ACTIVE,
    INACTIVE,
    DRAFT
}
