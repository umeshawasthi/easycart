package com.easycart.core.data.category;

import com.easycart.core.data.BaseDto;
import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class LocCategoryDto implements BaseDto, Serializable {
    private String name;
    private String description;
    private String isoCode;

    public LocCategoryDto(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
