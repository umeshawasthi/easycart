package com.easycart.core.data.product;

import com.easycart.core.data.BaseDto;
import com.easycart.core.jpa.entity.seo.SEOMeta;
import com.easycart.core.product.ProductStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductDto implements BaseDto, Serializable {

    @JsonIgnore
    @EqualsAndHashCode.Include
    private Long id;

    @EqualsAndHashCode.Include
    @NotNull(message = "code is required. We can't create category without code")
    private String code;

    private SEOMeta seoMeta;

    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;


    private String mainCategory;
    private String externalCode;
    private ProductStatus status;
    private String brandCode;
    private String brandName;

    Map<@NotNull  String, @NotNull LocProductDto> translation;

    public ProductDto(Long id, String code, String externalCode, SEOMeta seoMeta, ProductStatus status, LocalDateTime createdAt, LocalDateTime updatedAt) {
        this.id = id;
        this.code = code;
        this.externalCode=externalCode;
        this.seoMeta = seoMeta;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.mainCategory = mainCategory;
        this.status = status;
    }
}
