package com.easycart.core.data;

import lombok.*;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor @AllArgsConstructor
@Builder
public class PageData implements Serializable {
    private int page;
    private int size;
    private List<String> sort;
    //private Map<String, String> sort;
}
