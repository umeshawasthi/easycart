package com.easycart.core.data.brand;

import com.easycart.core.data.BaseDto;
import com.easycart.core.jpa.entity.seo.SEOMeta;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BrandDto implements BaseDto, Serializable {

    @JsonIgnore
    @EqualsAndHashCode.Include
    private Long id;

    @EqualsAndHashCode.Include
    @NotNull(message = "code is required. We can't create category without code")
    private String code;

    private SEOMeta seoMeta;

    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    Map<@NotNull  String, @NotNull LocBrandDto> translation;

    public BrandDto(Long id, String code) {
        this.id = id;
        this.code = code;
    }

    public BrandDto(Long id, String code, SEOMeta seoMeta) {
        this.id = id;
        this.code = code;
        this.seoMeta = seoMeta;
    }


    public BrandDto(Long id, String code, SEOMeta seoMeta, LocalDateTime createdAt, LocalDateTime updatedAt) {
        this.id = id;
        this.code = code;
        this.seoMeta = seoMeta;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }
}
