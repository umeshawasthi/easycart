package com.easycart.core.data.product;

import com.easycart.core.data.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class LocOptionValueDto implements BaseDto, Serializable {
    private String code;
    Map<@NotNull String, @NotNull LocOptionValueMapDto> translation;

}
