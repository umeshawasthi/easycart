package com.easycart.core.data.product;

import com.easycart.core.data.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class LocProductDto implements BaseDto, Serializable {

    private String name;
    private String description;
    private String isoCode;

    public LocProductDto(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
