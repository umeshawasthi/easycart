package com.easycart.core.data.product;

import com.easycart.core.data.BaseDto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OptionDto implements BaseDto, Serializable {

    @JsonIgnore
    @EqualsAndHashCode.Include
    private Long id;

    @EqualsAndHashCode.Include
    @NotNull(message = "code is required. We can't create category without code")
    private String code;

    private long position;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    private Map<@NotNull  String, @NotNull LocOptionDto> option;
    private List<LocOptionValueDto> values;

    public OptionDto(Long id, String code, long position) {
        this.id = id;
        this.code = code;
        this.position = position;
    }

    public OptionDto(Long id, String code, long position, LocalDateTime createdAt, LocalDateTime updatedAt) {
        this.id = id;
        this.code = code;
        this.position = position;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }
}
