package com.easycart.core.data.category;

import com.easycart.core.data.BaseDto;
import com.easycart.core.jpa.entity.seo.SEOMeta;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CategoryDto implements BaseDto, Serializable {

    @JsonIgnore
    @EqualsAndHashCode.Include
    private Long id;

    @EqualsAndHashCode.Include
    @NotNull(message = "code is required. We can't create category without code")
    private String code;
    private String externalCode;
    private SEOMeta seoMeta;
    private CategoryDto parent;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    Map<@NotNull  String, @NotNull LocCategoryDto> translation;


    public CategoryDto(Long id, String code) {
        this.id = id;
        this.code = code;
    }

    public CategoryDto(Long id, String code, String externalCode, SEOMeta seoMeta) {
        this.id = id;
        this.code = code;
        this.externalCode = externalCode;
        this.seoMeta=seoMeta;
    }

    public CategoryDto(Long id, String code, String externalCode, SEOMeta seoMeta, LocalDateTime createdAt, LocalDateTime updatedAt, String parent) {
        this.id = id;
        this.code = code;
        this.externalCode = externalCode;
        this.seoMeta=seoMeta;
        this.createdAt=createdAt;
        this.updatedAt=updatedAt;
        this.parent = CategoryDto.builder().code(parent).build();

    }
}
