package com.easycart.core.data.product;

import com.easycart.core.data.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class LocOptionDto implements BaseDto, Serializable {

    private String name;
    private String isoCode;

    public LocOptionDto(String name) {
        this.name = name;
    }
}
