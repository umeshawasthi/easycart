package com.easycart.core.services.product;

import com.easycart.core.convertor.product.option.LocProductOptionConvertor;
import com.easycart.core.convertor.product.option.ProductOptionConvertor;
import com.easycart.core.data.product.LocOptionDto;
import com.easycart.core.data.product.OptionDto;
import com.easycart.core.jpa.entity.product.options.ProductOption;
import com.easycart.core.jpa.repository.product.options.OptionRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service("optionService")
public class DefaultProductOptionService implements OptionService{

    @Resource
    private OptionRepository optionRepository;

    @Resource(name = "optionConvertor")
    private ProductOptionConvertor optionConvertor;

    @Resource
    private LocProductOptionConvertor locOptionConvertor;

    @Override
    public OptionDto createProductOption(OptionDto optionDto) {

        ProductOption option = optionConvertor.convertToEntity(optionDto);
        optionDto.getOption()
                .forEach((isoCode, oDto) ->{
                    oDto.setIsoCode(isoCode);
                    option.addLocalizedOption(isoCode, locOptionConvertor.convert(oDto));
                 });
        return optionConvertor.convertToDto(optionRepository.save(option));
    }

    @Override
    public OptionDto getOptionByCode(String code) {
        OptionDto dto = optionRepository.findOptionByCode(code);
        if(Objects.isNull(dto)){
            // throw and empty data with details
            return OptionDto.builder().build();
        }
        ProductOption option = new ProductOption();
        option.setId(dto.getId());
        List<LocOptionDto> locProductOptions = optionRepository.findLocalizedProductOptionsById(option);
        Map<String, LocOptionDto> translation = new HashMap<>();
        locProductOptions.forEach(
                e -> translation.put(e.getIsoCode(), e)
        );
        dto.setOption(translation);
        return dto;
    }

    @Override
    public Page<OptionDto> getAllOptions(Pageable pagination) {
       // return optionRepository.findAll(createPageRequest());
        return null;
    }

    private Pageable createPageRequest() {
        return PageRequest.of(0, 2, Sort.Direction.DESC,"code");
    }
}
