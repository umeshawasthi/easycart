package com.easycart.core.services.product;

import com.easycart.core.data.product.ProductDto;
import com.easycart.core.jpa.entity.product.Product;
import java.util.Collection;


public interface ProductService {

   Product createProduct(ProductDto product);
   Collection<Product> getAllProducts();
   ProductDto getProductByCode(String id, final String locale);
}
