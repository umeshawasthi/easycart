package com.easycart.core.services.product;

import com.easycart.core.convertor.product.LocalizedProductConvertor;
import com.easycart.core.convertor.product.ProductConvertor;
import com.easycart.core.data.product.LocProductDto;
import com.easycart.core.data.product.ProductDto;
import com.easycart.core.jpa.entity.product.Product;
import com.easycart.core.jpa.repository.core.LanguageRepository;
import com.easycart.core.jpa.repository.product.ProductRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.*;

@Service
public class DefaultProductService implements ProductService{
	
	@Resource
	ProductRepository productRepository;

	@Resource
	ProductConvertor productConvertor;

	@Resource
	LocalizedProductConvertor locProductConvertor;

	@Resource
	LanguageRepository languageRepository;


	public Product createProduct(@Valid ProductDto productData) {

		Product product = productConvertor.convertToEntity(productData);
		productData.getTranslation()
				.forEach((isoCode, productDto) -> {
					productDto.setIsoCode(isoCode);
					product.addLocProducts(isoCode, locProductConvertor.convert(productDto));
				});
		return productRepository.save(product);
	}

	public Collection<Product> getAllProducts() {
		return (Collection<Product>) productRepository.findAll();
	}

	@Override
	public ProductDto getProductByCode(String code, final String locale) {
		ProductDto productDto = productRepository.findProductByCode(code);
		if(Objects.nonNull(productDto)) {
			Product product = new Product();
			product.setId(productDto.getId());
			List<LocProductDto> locProducts = new ArrayList<>();
			if(StringUtils.isNotEmpty(locale)) {
				locProducts = productRepository.findLocalizedProductByLanguage(product, languageRepository.findByIsoCode(locale));
			}
			else{
				locProducts = productRepository.findLocalizedProductByCode(product);
			}
			Map<String, LocProductDto> translation = new HashMap<>();
			locProducts.stream().forEach(
					e -> translation.put(e.getIsoCode(), e)
			);
			productDto.setTranslation(translation);
			return productDto;
		}
		return ProductDto.builder().build();
	}

}
