package com.easycart.core.services;

import com.easycart.core.data.category.CategoryDto;
import com.easycart.core.jpa.entity.category.Category;
import org.springframework.data.domain.Page;

public interface CategoryService {

    Category createCategory(CategoryDto categoryData);
    CategoryDto getCategoryByCode(String code);
    Page<CategoryDto> getAllCategories();
}
