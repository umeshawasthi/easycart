package com.easycart.core.services.product;

import com.easycart.core.data.product.OptionDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface OptionService {

    OptionDto createProductOption(final OptionDto optionDto);
    OptionDto getOptionByCode(String code);
    Page<OptionDto> getAllOptions(Pageable page);
}
