package com.easycart.core.services;

import com.easycart.core.convertor.category.CategoryConvertor;
import com.easycart.core.convertor.category.LocalizedCategoryConvertor;
import com.easycart.core.data.category.CategoryDto;
import com.easycart.core.data.category.LocCategoryDto;
import com.easycart.core.jpa.entity.category.Category;
import com.easycart.core.jpa.repository.category.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class DefaultCategoryService implements CategoryService{
	
	@Autowired
	CategoryRepository categoryRepository;

	@Resource(name = "categoryConvertor")
	CategoryConvertor categoryConvertor;

	@Resource(name = "locCategoryConvertor")
	LocalizedCategoryConvertor localizedCategoryConvertor;

	@Override
	@Transactional
	public Category createCategory(CategoryDto categoryData) {
		Category category = categoryConvertor.convertToEntity(categoryData);
		categoryData.getTranslation()
				.forEach((isoCode, categoryDto) -> {
					categoryDto.setIsoCode(isoCode);
					category.addLocCategory(isoCode, localizedCategoryConvertor.convert(categoryDto));
				});
		return categoryRepository.save(category);

	}

	@Override
	@Transactional
	public Page<CategoryDto> getAllCategories() {
		Page<CategoryDto> categories=  categoryRepository.findAllCategories(createPageRequest());
		categories.stream().forEach(
				e-> e.setTranslation(localizedCategoryConvertor.populateLocalizedData(e.getId() ))
		);
		return categories;
	}


	@Override
	public CategoryDto getCategoryByCode(String code) {
		CategoryDto categoryDto =  categoryRepository.findCategoryByCode(code);
		if(Objects.nonNull(categoryDto)) {
			Category cat = new Category();
			cat.setId(categoryDto.getId());
			List<LocCategoryDto> locCategories = categoryRepository.findLocalizedCategoryById(cat);
			Map<String, LocCategoryDto> translation = new HashMap<>();
			locCategories.stream().forEach(
					e -> translation.put(e.getIsoCode(), e)
			);
			categoryDto.setTranslation(translation);
			return categoryDto;
		}
		return CategoryDto.builder().build();
	}

	private Pageable createPageRequest() {
		return PageRequest.of(0, 2, Sort.Direction.DESC,"code");
	}
}
