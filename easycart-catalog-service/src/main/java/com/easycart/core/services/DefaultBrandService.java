package com.easycart.core.services;


import javax.annotation.Resource;
import javax.transaction.Transactional;

import com.easycart.core.convertor.brand.BrandConvertor;
import com.easycart.core.convertor.brand.LocalizedBrandConvertor;
import com.easycart.core.data.brand.BrandDto;
import com.easycart.core.data.brand.LocBrandDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.easycart.core.jpa.entity.product.Brand;
import com.easycart.core.jpa.repository.brand.BrandRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class DefaultBrandService implements BrandService {
	
	@Resource
	BrandRepository brandRepository;


	@Resource(name = "brandConvertor")
	BrandConvertor brandConvertor;

	@Resource(name = "locBrandConvertor")
	LocalizedBrandConvertor localizedBrandConvertor;


	@Override
	@Transactional
	public Brand createBrand(BrandDto brandDto) {
		Brand brand = brandConvertor.convertToEntity(brandDto);
		brandDto.getTranslation()
				.forEach((isoCode, bDto) -> {
					bDto.setIsoCode(isoCode);
					brand.addLocBrand(isoCode, localizedBrandConvertor.convert(bDto));
				});
		return brandRepository.save(brand);
	}

	@Override
	public BrandDto getBrandByCode(String code) {
		BrandDto brandDto =  brandRepository.findBrandByCode(code);
		if(Objects.nonNull(brandDto)) {
			Brand brand = new Brand();
			brand.setId(brandDto.getId());
			List<LocBrandDto> locBrands = brandRepository.findLocalizedBrandById(brand);
			Map<String, LocBrandDto> translation = new HashMap<>();
			locBrands.forEach(
					e -> translation.put(e.getIsoCode(), e)
			);
			brandDto.setTranslation(translation);
			return brandDto;
		}
		return BrandDto.builder().build();
	}

	@Override
	public Page<BrandDto> getAllBrands() {
		Page<BrandDto> brands= brandRepository.findAllBrands(createPageRequest());
		brands.stream().forEach(
				e-> e.setTranslation(localizedBrandConvertor.populateLocalizedData(e.getId() ))
		);
		return brands;
	}

	private Pageable createPageRequest() {
		return PageRequest.of(0, 2, Sort.Direction.DESC,"code");
	}
}
