package com.easycart.core.services;

import com.easycart.core.data.brand.BrandDto;
import com.easycart.core.jpa.entity.product.Brand;
import org.springframework.data.domain.Page;

public interface BrandService {

    Brand createBrand(final BrandDto brandDto);
    BrandDto getBrandByCode(String code);
    Page<BrandDto> getAllBrands();
}
