package com.easycart.web.data;

import com.easycart.core.data.BaseDto;
import com.easycart.core.jpa.entity.seo.SEOMeta;
import lombok.*;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BrandData implements Serializable, BaseDto {

    @NonNull
    private String code;
    private String name;
    private SEOMeta seoMeta;
}
