package com.easycart.web.data;

import com.easycart.core.data.BaseDto;
import lombok.*;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OptionData implements Serializable, BaseDto {

    @NonNull
    private String code;
    private String name;
}
