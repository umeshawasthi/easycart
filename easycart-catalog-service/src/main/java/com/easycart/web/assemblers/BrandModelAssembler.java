package com.easycart.web.assemblers;


import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import com.easycart.core.data.BaseDto;
import com.easycart.core.data.PageData;
import com.easycart.core.data.brand.BrandDto;
import com.easycart.web.data.BrandData;
import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import com.easycart.web.controller.BrandController;

import java.util.ArrayList;
import java.util.List;

@Component
public class BrandModelAssembler implements RepresentationModelAssembler<BaseDto, EntityModel<? extends BaseDto>> {


  @Override
  public EntityModel<? extends BaseDto> toModel(BaseDto entity) {

    if(entity instanceof BrandDto) {
      BrandDto brand = (BrandDto) entity;
      if(StringUtils.isEmpty((brand.getCode()))){
        return EntityModel.of(brand);
      }
      return EntityModel.of(brand,buildLinks(brand));
    }
    BrandData brand = (BrandData) entity;
    return EntityModel.of(brand,buildLinks(brand));
  }

  private final List<Link> buildLinks(BrandDto brand){
    List<Link> links = new ArrayList<>();
    links.add(linkTo(methodOn(BrandController.class).getById(brand.getCode())).withSelfRel());
    links.add(linkTo(methodOn(BrandController.class).getAll(new PageData())).withRel("brand"));
    return links;
  }

  private final List<Link> buildLinks(BrandData brand ){
    List<Link> links = new ArrayList<>();
    links.add(linkTo(methodOn(BrandController.class).getById(brand.getCode())).withSelfRel());
    links.add(linkTo(methodOn(BrandController.class).getAll(new PageData())).withRel("brand"));

    return links;
  }


}