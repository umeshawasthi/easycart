package com.easycart.web.assemblers.product;

import com.easycart.core.data.BaseDto;
import com.easycart.core.data.PageData;
import com.easycart.core.data.product.OptionDto;
import com.easycart.web.controller.BrandController;
import com.easycart.web.controller.ProductOptionController;
import com.easycart.web.data.OptionData;
import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class ProductOptionModelAssembler implements RepresentationModelAssembler<BaseDto, EntityModel<? extends BaseDto>> {


    @Override
    public EntityModel<? extends BaseDto> toModel(BaseDto entity) {

        if(entity instanceof OptionDto) {
            OptionDto option = (OptionDto) entity;
            if(StringUtils.isEmpty((option.getCode()))){
                return EntityModel.of(option);
            }
            return EntityModel.of(option,buildLinks(option));
        }
        OptionData option = (OptionData) entity;
        return EntityModel.of(option,buildLinks(option));
    }

    private final List<Link> buildLinks(OptionDto option){
        List<Link> links = new ArrayList<>();
        links.add(linkTo(methodOn(ProductOptionController.class).getById(option.getCode())).withSelfRel());
        links.add(linkTo(methodOn(ProductOptionController.class).getAll(new PageData())).withRel("options"));
        return links;
    }

    private final List<Link> buildLinks(OptionData option ){
        List<Link> links = new ArrayList<>();
        links.add(linkTo(methodOn(BrandController.class).getById(option.getCode())).withSelfRel());
        links.add(linkTo(methodOn(BrandController.class).getAll(new PageData())).withRel("options"));

        return links;
    }

}
