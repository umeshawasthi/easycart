package com.easycart.web.assemblers;


import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import com.easycart.core.data.BaseDto;
import com.easycart.core.data.PageData;
import com.easycart.core.data.category.CategoryDto;
import com.easycart.web.data.CategoryData;
import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import com.easycart.web.controller.CategoryController;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class CategoryModelAssembler implements RepresentationModelAssembler<BaseDto, EntityModel<? extends BaseDto>> {


  @Override
  public EntityModel<? extends BaseDto> toModel(BaseDto entity) {

    if(entity instanceof CategoryDto) {
      CategoryDto category = (CategoryDto) entity;
      if(StringUtils.isEmpty((category.getCode()))){
        return EntityModel.of(category);
      }
      return EntityModel.of(category,buildLinks(category));
    }
    CategoryData category = (CategoryData) entity;
    return EntityModel.of(category,buildLinks(category));
  }

  private final List<Link> buildLinks(CategoryDto category){
    List<Link> links = new ArrayList<>();
    links.add(linkTo(methodOn(CategoryController.class).getById(category.getCode())).withSelfRel());
    links.add(linkTo(methodOn(CategoryController.class).getAll(new PageData())).withRel("category"));

    if(Objects.nonNull(category.getParent())){
      links.add(linkTo(methodOn(CategoryController.class).getById(category.getParent().getCode())).withRel("parent"));
    }
    return links;
  }

  private final List<Link> buildLinks(CategoryData category ){
    List<Link> links = new ArrayList<>();
    links.add(linkTo(methodOn(CategoryController.class).getById(category.getCode())).withSelfRel());
    links.add(linkTo(methodOn(CategoryController.class).getAll(new PageData())).withRel("category"));

    if(Objects.nonNull(category.getParent())){
      links.add(linkTo(methodOn(CategoryController.class).getById(category.getParent().getCode())).withRel("parent"));
    }
    return links;
  }

}