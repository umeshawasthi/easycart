package com.easycart.web.assemblers;


import com.easycart.core.data.BaseDto;
import com.easycart.core.data.PageData;
import com.easycart.core.data.product.ProductDto;
import com.easycart.web.controller.ProductController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class ProductModelAssembler implements RepresentationModelAssembler<BaseDto, EntityModel<? extends BaseDto>> {


  @Override
  public EntityModel<? extends BaseDto> toModel(BaseDto entity) {
    if(entity instanceof ProductDto){
      ProductDto productDto = (ProductDto) entity;
      if(StringUtils.isEmpty((productDto.getCode()))){
        return EntityModel.of(productDto);
      }
      return EntityModel.of(productDto,buildLinks(productDto));
    }
    return null;
  }

  private final List<Link> buildLinks(ProductDto product){
    List<Link> links = new ArrayList<>();
    links.add(linkTo(methodOn(ProductController.class).getById(product.getCode(), "en_US")).withSelfRel());
    links.add(linkTo(methodOn(ProductController.class).getAll(new PageData())).withRel("products"));
    return links;
  }
}