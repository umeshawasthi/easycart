package com.easycart.web.controller;


import com.easycart.core.convertor.product.ProductConvertor;
import com.easycart.core.data.PageData;
import com.easycart.core.data.product.ProductDto;
import com.easycart.core.jpa.entity.product.Product;
import com.easycart.core.services.product.ProductService;
import com.easycart.web.assemblers.ProductModelAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

@RestController
@RequestMapping(value = "/products", produces = "application/hal+json")
public class ProductController {

	@Resource
	private ProductService productService;

	@Resource
	private ProductModelAssembler assembler;

	@Resource
	private ProductConvertor productConvertor;
	
	@GetMapping 
	public ResponseEntity getAll(PageData pageData) {
		
		/* List<EntityModel<Product>> brands = productService.getAllProducts().stream() //
			      .map(assembler::toModel) //
			      .collect(Collectors.toList());
			  return CollectionModel.of(brands); */
		return null;
	}
	
	@GetMapping(path = "/product/{id}")
	public ResponseEntity<EntityModel<ProductDto>> getById(@PathVariable String id, @RequestParam(value = "locale", required = false) String locale) {
		 ProductDto product = productService.getProductByCode(id, locale);
		 EntityModel<ProductDto> entity = (EntityModel<ProductDto>) assembler.toModel(product);
		return new ResponseEntity(entity, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<EntityModel<ProductDto>> create(@Valid @RequestBody ProductDto productData) {

		 Product product = productService.createProduct(productData);
		EntityModel<ProductDto> entity= (EntityModel<ProductDto>) assembler.toModel(productConvertor.convertToDto(product));
		return new ResponseEntity(entity, HttpStatus.CREATED);
		//return null;
	}
}

