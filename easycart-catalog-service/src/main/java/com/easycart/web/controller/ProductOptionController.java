package com.easycart.web.controller;

import com.easycart.core.data.PageData;
import com.easycart.core.data.product.OptionDto;
import com.easycart.core.jpa.entity.product.options.ProductOption;
import com.easycart.core.services.product.OptionService;
import com.easycart.web.assemblers.product.ProductOptionModelAssembler;
import com.easycart.web.data.BrandData;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

@RestController
@RequestMapping(value = "/product-options", produces = "application/hal+json")
public class ProductOptionController {

    @Resource
    private OptionService optionService;

    @Resource
    private PagedResourcesAssembler<OptionDto> pagedResourcesAssembler;

    @Resource
    private ProductOptionModelAssembler optionAssembler;

    @PostMapping
    public ResponseEntity<EntityModel<ProductOption>> create(@Valid @RequestBody OptionDto optionDto) {

        OptionDto option=  optionService.createProductOption(optionDto);
        EntityModel<BrandData> entity= (EntityModel<BrandData>) optionAssembler.toModel(option);
        return new ResponseEntity(entity, HttpStatus.CREATED);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<EntityModel> getById(@PathVariable String id) {
        OptionDto option = optionService.getOptionByCode(id);
        EntityModel<OptionDto> entity= (EntityModel<OptionDto>) optionAssembler.toModel(option);
        return new ResponseEntity(entity, HttpStatus.OK);
    }

    @GetMapping
    @Cacheable(cacheNames = "optionList", key = "1")
    public ResponseEntity getAll(PageData pageData) {
        return new ResponseEntity(pagedResourcesAssembler.toModel(null), HttpStatus.OK);
    }

}
