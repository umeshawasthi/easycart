package com.easycart.web.controller;



import com.easycart.core.convertor.brand.BrandConvertor;
import com.easycart.core.data.PageData;
import com.easycart.core.data.brand.BrandDto;
import com.easycart.core.jpa.entity.product.Brand;
import com.easycart.core.services.BrandService;
import com.easycart.web.assemblers.BrandModelAssembler;
import com.easycart.web.data.BrandData;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

@RestController
@CacheConfig(cacheNames = {"brand"})
@RequestMapping(value = "/brands", produces = "application/hal+json")
public class BrandController {

	@Resource
	private BrandService brandService;

	@Resource
	private BrandModelAssembler brandAssembler;

	@Resource
	private PagedResourcesAssembler<BrandDto> pagedResourcesAssembler;


	@Resource
	BrandConvertor brandConvertor;
	
	@GetMapping
	@Cacheable(cacheNames = "brandList", key = "1")
	public ResponseEntity getAll(PageData pageData) {

		Page<BrandDto> brands = brandService.getAllBrands();
		return new ResponseEntity(pagedResourcesAssembler.toModel(brands), HttpStatus.OK);
	}
	
	@GetMapping(path = "/{id}")
	public ResponseEntity<EntityModel<Brand>> getById(@PathVariable String id) {
		BrandDto brand = brandService.getBrandByCode(id);

		EntityModel<BrandDto> entity= (EntityModel<BrandDto>) brandAssembler.toModel(brand);
		return new ResponseEntity(entity, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<EntityModel<BrandData>> create(@Valid @RequestBody BrandDto brandDto) {
		Brand brand = brandService.createBrand(brandDto);
		EntityModel<BrandData> entity= (EntityModel<BrandData>) brandAssembler.toModel(brandConvertor.convertToDto(brand));
		return new ResponseEntity<>(entity, HttpStatus.CREATED);
	}
}

