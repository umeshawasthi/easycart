package com.easycart.web.controller;


import com.easycart.core.convertor.category.CategoryConvertor;
import com.easycart.core.data.PageData;
import com.easycart.core.data.category.CategoryDto;
import com.easycart.core.jpa.repository.core.LanguageRepository;
import com.easycart.core.services.CategoryService;
import com.easycart.web.data.CategoryData;
import org.springframework.data.domain.Page;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.easycart.web.assemblers.CategoryModelAssembler;
import com.easycart.core.jpa.entity.category.Category;

import javax.annotation.Resource;
import javax.validation.Valid;

@RestController
@RequestMapping(value = "/categories")
public class CategoryController {

	@Resource
	private CategoryService categoryService;

	@Resource
	private CategoryModelAssembler assembler;

	@Resource(name = "categoryConvertor")
	CategoryConvertor categoryConvertor;

	@Resource
	LanguageRepository languageRepository;

	@Resource
	private PagedResourcesAssembler<CategoryDto> pagedResourcesAssembler;
	
	@GetMapping
	public ResponseEntity getAll(PageData pageData) {

		Page<CategoryDto> categories = categoryService.getAllCategories();
		return new ResponseEntity(pagedResourcesAssembler.toModel(categories), HttpStatus.OK);
	}
	
	@GetMapping(path = "/{id}")
	public ResponseEntity<EntityModel<CategoryDto>> getById(@PathVariable String id) {
		CategoryDto category = categoryService.getCategoryByCode(id);
		EntityModel<CategoryDto> entity= (EntityModel<CategoryDto>) assembler.toModel(category);
		return new ResponseEntity(entity, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<EntityModel<CategoryData>> create(@Valid  @RequestBody CategoryDto categoryData) {
		Category category = categoryService.createCategory(categoryData);
		EntityModel<CategoryData> entity= (EntityModel<CategoryData>) assembler.toModel(categoryConvertor.convertToDto(category));
		return new ResponseEntity<>(entity, HttpStatus.CREATED);	
	}
}

