package com.easycart.web.error;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;


import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder(setterPrefix = "with")
public class ApiErrorResponse implements Serializable {

    private HttpStatus status;
    private String message;
    private String description;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timeStamp;
    private String errorCode;
}
