package com.easycart.web.handlers;

import com.easycart.web.error.ApiErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;

@ControllerAdvice
@Slf4j
public class RESTExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler({Exception.class})
    public ResponseEntity <ApiErrorResponse> customerNotFound(Exception ex, WebRequest request) {
        ApiErrorResponse apiResponse = ApiErrorResponse.builder()
                 .withStatus(HttpStatus.NOT_FOUND)
                .withDescription(ex.getMessage())
                .withErrorCode("404")
                .withTimeStamp(LocalDateTime.now())
                .build();
        return new ResponseEntity<ApiErrorResponse>(apiResponse, HttpStatus.NOT_FOUND);

        //We can define other handlers based on Exception types
    }

    @Override
    public ResponseEntity handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ApiErrorResponse apiResponse = ApiErrorResponse.builder()
                .withStatus(status)
                .withMessage(ex.getBindingResult().getFieldError().getDefaultMessage())
                .withDescription(ex.getMessage())
                .withTimeStamp(LocalDateTime.now())
                .build();
        return  ResponseEntity.badRequest().body(apiResponse);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public final ResponseEntity<ApiErrorResponse> handleConstraintViolationException(Exception ex, WebRequest request) {
        ApiErrorResponse apiResponse = ApiErrorResponse.builder()
                .withStatus(HttpStatus.BAD_REQUEST)
                .withMessage(ex.getMessage())
                .withDescription(ex.getMessage())
                .withTimeStamp(LocalDateTime.now())
                .build();

        return  ResponseEntity.badRequest().body(apiResponse);
    }
}
