package com.easycart.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.Optional;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditProvider")
public class JPAConfiguration {

    @Bean
    public AuditorAware<String> auditProvider() {

        /*
          SecurityContextHolder.getContext().getAuthentication().getName()
         */
        return () -> Optional.of("easyCart");
    }
}
